# -*- coding: utf-8 -*-
import datetime
import psycopg2 as pg
import psycopg2.extras
import dateutil.relativedelta

""" --------------- 1. Подключение к бд --------------- """
dialerdb = pg.connect(dbname='dialerdb', user='postgres', password='', host='localhost')
cursor = dialerdb.cursor(cursor_factory=psycopg2.extras.DictCursor)

""" --------------- 2. Получение сегодняшней даты, последнего дня предыдущего месяца и вчерашнаяя дата --------------- """
dtDate = datetime.datetime.today()
# получение даты начала и конец прошлого месяца
lastMonth = (dtDate + dateutil.relativedelta.relativedelta(months=-1))
lastDate = datetime.datetime(lastMonth.year, lastMonth.month, (dtDate - datetime.timedelta(days=dtDate.day)).day, 23, 59, 59)
table_name = 'statistic_' + str(dtDate.month) + '_' + str(dtDate.year)
#получение даты прошедшего дня
tom = dtDate + dateutil.relativedelta.relativedelta(days=-1)
tomorrow = datetime.datetime(tom.year, tom.month, tom.day, 23, 59, 59)

""" --------------- 3. Прверка/запись таблицы текущего месяца --------------- """
cursor.execute('CREATE TABLE if not exists statistic_' + str(dtDate.month) + '_' + str(dtDate.year) + ' (id integer, login text, day integer, time_work character varying(50), record integer, salary integer)', )
dialerdb.commit()

""" --------------- 4. Получение всех записей --------------- """
cursor.execute('SELECT * FROM crm_timework WHERE d='+str(dtDate.day)) #str(id)  str(tomorrow)
crm_timework = cursor.fetchall()
#таблица продукта с баллами
cursor.execute('SELECT id, ball FROM prod ORDER BY id')
list_balls = cursor.fetchall()
#print(list_balls)

# 6. получение логина и количества записей
def info(id):
    cursor.execute('SELECT crm_leads.id_agent, crm_user.login FROM public.crm_leads JOIN crm_user ON (crm_leads.id_agent=crm_user.id) Where crm_leads.id_agent=%s and crm_leads.date_add > %s', (str(id), str(tomorrow),))
    record = cursor.fetchall()
    return record

# 7. проверка существования записи агента
def CheckOper(id):
    cursor.execute('SELECT id, day FROM statistic_' + str(dtDate.month) + '_' + str(dtDate.year) + ' WHERE id=' + '\'' + str(id) + '\' and day=%s', (dtDate.day,))
    oper = cursor.fetchall()
    if(oper == []):
        return 0
    else:
        return 1

def salary_check(ball):
    if(ball <= 100): return 40*ball
    elif (ball > 100 and ball <= 125):return 5000
    elif (ball > 125 and ball <= 150): return 5000
    elif (ball > 150 and ball <= 175): return 6000
    elif (ball > 175 and ball <= 200): return 7000
    elif (ball > 200 and ball <= 225): return 8000
    elif (ball > 225 and ball <= 250): return 9000
    elif (ball > 250 and ball <= 275): return 10000
    elif (ball > 275 and ball <= 300): return 11000
    elif (ball > 300): return 12000

""" --------------- 5. Получение, создание / обновление записей --------------- """
for i in range(len(crm_timework)):
    id = crm_timework[i][4] #id
    day = dtDate.day    # текущий день
    time_work = crm_timework[i][8]  # время работы за день
    infor = info(id) #получение массива с логином и количеством записей
    if(infor == []):
        continue
    login = infor[0][1] #login
    record = len(infor) #количество записей
    """balls = 0
    for i in range(len(infor)):
        for k in range(len(list_balls)):
            if(list_balls[k][0] == infor[i][2]):
                balls += list_balls[k][1]
                break
            else: continue
    #print(salary_check(balls))"""
    salary = salary_check(record) #примерная базовая ставка
    #если запись есть - обновление, если нет - создание записи
    flag = CheckOper(id)
    if(flag==1):
        cursor.execute('UPDATE statistic_' + str(dtDate.month) + '_' + str(dtDate.year) + ' SET id=%s,login=%s,day=%s,time_work=%s,record=%s, salary=%s WHERE login=%s and day=%s',
                          (str(id), str(login), str(day), str(time_work), str(record), str(salary),
                           str(login), str(day)))
        dialerdb.commit()
    elif(flag==0):
        cursor.execute('INSERT INTO statistic_' + str(dtDate.month) + '_' + str(dtDate.year) + ' VALUES (%s, %s, %s, %s, %s, %s)', (id, login, day, time_work, record, salary))
        dialerdb.commit()