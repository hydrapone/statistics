# -*- coding: utf-8 -*-
import datetime
import psycopg2 as pg
import psycopg2.extras
import dateutil.relativedelta

""" --------------- 1. Подключение к бд --------------- """
dialerdb = pg.connect(dbname='dialerdb', user='postgres', password='', host='localhost')
cursor = dialerdb.cursor(cursor_factory=psycopg2.extras.DictCursor)
#получения таблицы сотношения продукта/баллов
#cursor.execute('SELECT id, ball FROM prod ORDER BY id')
#list_balls = cursor.fetchall()

""" --------------- 2. Получение первой записи --------------- """
cursor.execute('SELECT oper, d, m, y, time_work FROM crm_timework order by y, m, d')
first = cursor.fetchone()

""" --------------- 3. Получение даты первой записи --------------- """
month = int(first[2])
year = int(first[3])

#Получение следующего месяца
def CheckNextMonth(date):
    if(date == 12): return 1
    else: return date+1

""" --------------- 5.1 Таблица соотношения ставки/баллов --------------- """
def salary_check(ball):
    if(ball <= 100): return 40*ball
    elif (ball > 100 and ball <= 125):return 4000
    elif (ball > 125 and ball <= 150): return 5000
    elif (ball > 150 and ball <= 175): return 6000
    elif (ball > 175 and ball <= 200): return 7000
    elif (ball > 200 and ball <= 225): return 8000
    elif (ball > 225 and ball <= 250): return 9000
    elif (ball > 250 and ball <= 275): return 10000
    elif (ball > 275 and ball <= 300): return 11000
    elif (ball > 300): return 12000

""" --------------- 4. Получение дат для запросов бд --------------- """
date = str(month) + '_' + str(year)
d = first[1]
m = first[2]
y = first[3]

res = []
cout = 0
""" --------------- 5. Цикл работающий до текущего времени --------------- """
while(date != '4_2019'):
    # Получение диапазона дат для запроса
    dtDate = datetime.datetime(y, CheckNextMonth(m), 1)
    lastMonth = (dtDate + dateutil.relativedelta.relativedelta(months=-1))
    lastDate = datetime.datetime(lastMonth.year, lastMonth.month, (dtDate - datetime.timedelta(days=dtDate.day)).day, 23, 59, 59)
    #print(lastMonth, '\n')
    #print(lastDate, '\n')
    cursor.execute('SELECT oper, d, m, y, time_work FROM crm_timework where m=%s and y=%s order by y, m, d ', (str(m), str(y),))
    info = cursor.fetchall()
    if(info != []):
        for i in range(len(info)):
            cursor.execute('SELECT id_agent, date_add FROM public.crm_leads where date_add > %s and date_add < %s and id_agent = %s', (lastMonth, lastDate, info[i][0]))
            crm_leads = cursor.fetchall()
            for k in range(len(crm_leads)):
                if(info[i][1] == crm_leads[k][1].day):
                    cout+=1
            balls = 0
            """else:
                for n in range(len(crm_leads)):
                    if (info[i][1] == crm_leads[n][1].day):
                        for j in range(len(list_balls)):
                            if (list_balls[j][0] == crm_leads[n][2]):
                                balls += list_balls[j][1]
                                break
                salary = salary_check(balls)  # примерная базовая ставка"""
            salary = salary_check(cout)
            cursor.execute('SELECT login FROM crm_user WHERE id = %s', (info[i][0],))
            login = cursor.fetchone()[0]
            inf = [info[i][0], login, info[i][1], str(info[i][4]), cout, salary]
            res.append(inf)
            cout = 0
        cursor.execute('CREATE TABLE if not exists statistic_' + str(m) + '_' + str(y) + ' (id integer, login text, day integer, time_work character varying(50), record integer, salary text)', )
        dialerdb.commit()
        for i in range(len(info)):
            cursor.execute('INSERT INTO statistic_' + str(m) + '_' + str(y) + ' VALUES (%s, %s, %s, %s, %s, %s)',(str(res[i][0]), str(res[i][1]), str(res[i][2]), str(res[i][3]), str(res[i][4]), str(res[i][5]),))
            dialerdb.commit()

    m = CheckNextMonth(m)
    if(m == 1):
        y += 1
    date = str(m) + '_' + str(y)
    res = []